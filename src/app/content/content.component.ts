import { Component} from '@angular/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent {
  title = '';
  url = '';
  showForm = false;
  userPassword = '';

  content = [
    {title: 'image 1', url: 'https://i.pinimg.com/564x/b2/ca/3f/b2ca3f3f453e39be8b63a78cf8a2d151.jpg'},
    {title: 'image 2', url: 'https://i.pinimg.com/564x/e9/91/f8/e991f8996fa7cc0bfaeef83d71915fa0.jpg'},
    {title: 'image 3', url: 'https://i.pinimg.com/564x/17/64/46/176446bf1e72d49298352b8a171de90c.jpg'},
  ];

  getPasswordInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.userPassword = target.value;
  }

  checkPassword() {
    if (this.userPassword === '123') {
      this.showForm = !this.showForm;
    } else {
      alert('Введите правильный пароль: 123')
    }
  }

  getTitle(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.title = target.value;
  }

  getUrl(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.url = target.value;
  }

  addImage() {
    this.content.push({
      title: this.title,
      url: this.url,
    });

  }

}
