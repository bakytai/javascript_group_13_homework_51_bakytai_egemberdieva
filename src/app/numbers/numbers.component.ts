import { Component} from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number[] = [];

  constructor() {
    this.numbers = this.generateNumbers();
  }

  generateNumbers() {
    const numberArray: number[] = [];
      for (let i = 1; i <= 5; i++) {
        const random = Math.floor(Math.random() * (36 - 5 + 1)) + 5;
        if (!numberArray.includes(random)) {
            numberArray.push(random);
        } else {
          i--
        }
        numberArray.sort(function (a,b) {
          return a - b
        });
      }
     return numberArray
  }

  getNewArrayNumbers() {
    this.numbers = this.generateNumbers();
  }
}
